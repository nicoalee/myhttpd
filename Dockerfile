FROM httpd:2.4

LABEL maintainer="Nicholas Lee"
LABEL email="niconal902@gmail.com"
LABEL version="0.1"

RUN apt-get -y update
RUN apt -y install mysql-server
COPY index.html /usr/local/apache2/htdocs
COPY start.sh /start.sh
RUN chmod +x /start.sh

ENTRYPOINT ["/start.sh"]

